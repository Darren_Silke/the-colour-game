# The Colour Game

The Colour Game is a front-end only Web application where a user has to try and guess the correct colour based on the given RGB value. It was built using HTML, CSS and JavaScript.

A live version of The Colour Game can be viewed [here](https://thecolourgame.netlify.app).

## Running Locally

Clone the repository and open the 'index.html' file in your Web browser.
